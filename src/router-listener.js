const listeners = []

/**
 * 添加监听器
 * @param {Function} listener
 */
export function addListener (listener) {
  listeners.push(listener)
}

/**
 * 移除监听器
 * @param {Function} listener
 */
export function removeListener (listener) {
  listeners.splice(listeners.indexOf(listener), 1)
}

/**
 * 通知所有监听器
 * @param to 即将要进入的目标 路由对象
 * @param from 当前导航正要离开的路由
 */
export function notify (to, from) {
  listeners.forEach(l => {
    l(to, from)
  })
}
