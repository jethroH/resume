import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Resume from '@/views/Resume'
import SamplePhoto from '@/views/SamplePhoto'
import VueLazyload from 'vue-lazyload'
import { notify } from '@/router-listener'

Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Resume
  },
  {
    path: '/sample',
    component: SamplePhoto
  }
]

const router = new VueRouter({
  routes // (缩写) 相当于 routes: routes
})

router.beforeEach((to, from, next) => {
  notify(to, from)
  next()
})

const loadImage = require('./assets/loading.png')
Vue.use(VueLazyload, {
  preLoad: 1.3,
  loading: loadImage,
  attempt: 1
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
